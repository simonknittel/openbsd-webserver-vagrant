#!/bin/ksh

echo ""
echo "setup.sh started"


cd /var/www/htdocs
rm -rf bgplg

echo "Creating openbsd.local"
mkdir openbsd.local
chmod 644 openbsd.local
chown root:wheel openbsd.local


echo "Creating index.html"
cd /var/www/htdocs/openbsd.local
echo "It works!" > index.html
chmod 644 index.html
chown root:wheel index.html


# cd /var/www/htdocs
# ln -s /var/www/htdocs/openbsd.local /var/www/htdocs/www.openbsd.local
# chmod 644 www.openbsd.local
# chown root:wheel www.openbsd.local


echo "Creating httpd.conf"
cd /etc
cat <<EOF > httpd.conf
ext_ip="192.168.30.1"

server "default" {
    listen on \$ext_ip port 80
}

server "openbsd.local" {
    listen on \$ext_ip port 80
    directory {
        auto index,
        index "index.html"
    }
    root "/htdocs/openbsd.local"
}

server "www.openbsd.local" {
    listen on \$ext_ip port 80
    directory {
        auto index,
        index "index.html"
    }
    root "/htdocs/openbsd.local"
}

types {
    include "/usr/share/misc/mime.types"
}

EOF
chmod 644 httpd.conf
chown root:wheel httpd.conf


echo "Starting httpd"
httpd


echo "setup.sh finished"
